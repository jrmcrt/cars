const keys = {
	direction: {
		left: 37,
		right: 39,
		up: 38,
		down: 40 
	},
	fire: [32, 17],
	pause: [27, 80],
	enter: 13
};

const sounds = {
	register: function(){
		for(var v of Object.keys(this))
			if(typeof this[v] !== 'function')
				createjs.Sound.registerSound('img/' + this[v], v);
	}
};

const imgs = {
	'car': 'PNG/Cars/car_black_small_1.png'
};
