function init() {

	var stage = new createjs.Stage("canvas");
	const maxPower = 0.5;
	const maxReverse = 0.0375;
	const powerFactor = 0.03;
	const reverseFactor = 0.0005;

	const drag = 0.95;
	const angularDrag = 0.95;
	const turnSpeed = 0.002;
	const pi = Math.PI;

	var game = {
		setupManifest: function(images){
		    for(let i in images)
				typeof images[i] === 'string' ? this.manifest.push({src: 'img/' + images[i]}) : this.setupManifest(images[i]);
		},

		_preload: function(){
			this.manifest = [];
			this.setupManifest(imgs);
			let preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);
			preload.on("fileload", this.handleFileLoad);
			preload.on("complete", this.start.bind(this));
			preload.on("error", this.loadError);
			preload.loadManifest(this.manifest);
		},

		handleFileLoad: function(event){
			console.log(`File loaded -> ${event.item.src}`);
		},

		loadError: function(event){
			console.error(`[HMLT5Gaming2] Could not load -> ${event.item.src}`);
		},


		start: function(){
			document.onkeydown = handleKeyDown;
			document.onkeyup = handleKeyUp;
			createjs.Ticker.addEventListener("tick", handleTick);
			createjs.Ticker.setFPS(60);
			sounds.register();
			car.append();
		},

	};

	var car = {
		power: 0,
		angle: 0,
		vx: 0,
		vy:0,
		angularVelocity: 0,
		direction: {left: false, right: false, up: false, down: false},

		append: function(){
			this.image = 'img/' + imgs.car;
			this.bitmap = new createjs.Bitmap(this.image);
			this.carContainer = new createjs.Container();
			this.carContainer.addChild(this.bitmap);
			stage.addChild(this.carContainer);
			this.carContainer.x = (stage.canvas.width / 2) - (this.bitmap.image.width / 2);
			this.carContainer.y = stage.canvas.height - 250;
			this.carContainer.regX = - this.bitmap.image.width / 2;
			this.carContainer.regY = this.bitmap.image.height / 2;
			console.log(this.bitmap, this.bitmap.image.width)
		}, 

		move: function(dir){
			if(dir === 'left' && this.power > 0.05){//still room for improvements
				this.angularVelocity -= turnSpeed * (this.power > 0.05);
			}
			else if(dir === 'right' && this.power > 0.05){
				this.angularVelocity += turnSpeed * (this.power > 0.05);
			}
			else if(dir === 'up' && this.carContainer.y > 5){
				this.power += powerFactor;
			}
			else if(dir === 'down' && this.carContainer.y < (stage.canvas.height - this.bitmap.image.height) - 5){
				this.power -= powerFactor * 4;
			}
		}

	};

	game._preload();

	function handleTick(event){
		for(var v in car.direction)
			if(car.direction[v])
				car.move(v);
/*		car.carContainer.rotation -= 2.5 ;
*/
		car.power = Math.max(0, Math.min(maxPower, car.power));
		car.vx += Math.sin(car.angle) * (car.power);
		car.vy += Math.cos(car.angle) * (car.power);
		car.vx *= drag;//friction
		car.vy *= drag;
		console.log(car.angle)
		car.carContainer.x += car.vx;
		car.carContainer.y -= car.vy;
		car.angularVelocity *= drag
		car.angle += car.angularVelocity;
		car.bitmap.rotation = (car.angle) * (180/pi);
		console.log(car.bitmap.rotation)
		car.power *= 0.98;//friction
		stage.update()
	}

	function handleKeyDown(e){
		var key = e.keyCode;
		for(var v of Object.keys(keys.direction))
			if(key == keys.direction[v])
				car.direction[v] = true;

		if(keys.fire.includes(key))
			car.firing = true;
	}

	function handleKeyUp(e){
		var key = e.keyCode;
		for(var v of Object.keys(keys.direction))
			if(key == keys.direction[v])
				car.direction[v] = false;

		if(keys.fire.includes(key))
			car.firing = false;	
	}

}
